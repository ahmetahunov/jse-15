package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.api.service.EntityManagerService;
import ru.ahmetahunov.tm.dto.SessionDTO;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.repository.SessionRepository;
import ru.ahmetahunov.tm.util.SessionSignatureUtil;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

	public SessionService(@NotNull final EntityManagerService entityManagerService) {
		super(entityManagerService);
	}

	@Override
	public void persist(@Nullable final Session session) {
		if (session == null) return;
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		entityManager.getTransaction().begin();
		repository.persist(session);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public void merge(@Nullable final Session session) {
		if (session == null) return;
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		entityManager.getTransaction().begin();
		repository.merge(session);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id, @Nullable final String userId) {
		if (id == null || id.isEmpty()) return null;
		if (userId == null || userId.isEmpty()) return null;
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		@Nullable final Session session = repository.findOneByUserId(id, userId);
		entityManager.close();
		return session;
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id) {
		if (id == null || id.isEmpty()) return null;
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		@Nullable final Session session = repository.findOne(id);
		entityManager.close();
		return session;
	}

	@NotNull
	@Override
	public List<Session> findAll() {
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		@NotNull final List<Session> sessions = repository.findAll();
		entityManager.close();
		return sessions;
	}

	@NotNull
	@Override
	public List<Session> findAll(@Nullable final String userId) {
		if (userId == null || userId.isEmpty()) return Collections.emptyList();
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		@NotNull final List<Session> sessions = repository.findAll(userId);
		entityManager.close();
		return sessions;
	}

	@Override
	public void remove(@Nullable final String id, @Nullable final String userId) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return;
		if (userId == null || userId.isEmpty()) return;
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		entityManager.getTransaction().begin();
		repository.removeByUserId(id, userId);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public void remove(@Nullable final String id) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return;
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		entityManager.getTransaction().begin();
		repository.remove(id);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public void validate(@Nullable final SessionDTO session)
			throws AccessForbiddenException, InterruptedOperationException {
		if (session == null) throw new AccessForbiddenException();
		@NotNull final EntityManager entityManager = getEntityManager();
		@NotNull final ISessionRepository repository = new SessionRepository(entityManager);
		@Nullable final Session checkSession = repository.findOne(session.getId());
		entityManager.close();
		if (checkSession == null || checkSession.getSignature() == null)
			throw new AccessForbiddenException();
		@Nullable final String signature = session.getSignature();
		session.setSignature(null);
		@Nullable final String hash = SessionSignatureUtil.sign(session);
		if (!checkSession.getSignature().equals(hash) || !signature.equals(hash))
			throw new AccessForbiddenException();
		final long existTime = System.currentTimeMillis() - session.getTimestamp();
		if (existTime > 32400000) {
			repository.remove(session.getId());
			throw new AccessForbiddenException("Access denied! Session has expired.");
		}
	}

	@Override
	public void validate(@Nullable final SessionDTO session, @NotNull final Role role)
			throws AccessForbiddenException, InterruptedOperationException {
		validate(session);
		if (!role.equals(session.getRole())) throw new AccessForbiddenException();
	}

}
