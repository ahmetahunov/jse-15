package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import ru.ahmetahunov.tm.dto.SessionDTO;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {

	private ServiceLocator serviceLocator;

	private IPropertyService propertyService;

	private IProjectService projectService;

	private ISessionService sessionService;

	public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
		this.propertyService = serviceLocator.getPropertyService();
		this.projectService = serviceLocator.getProjectService();
		this.sessionService = serviceLocator.getSessionService();
	}

	@Nullable
	@Override
	@WebMethod
	public ProjectDTO createProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "project") final ProjectDTO projectDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		if (projectDTO == null) throw new InterruptedOperationException();
		projectDTO.setUserId(sessionDTO.getUserId());
		projectService.persist(projectDTO.transformToEntity(serviceLocator));
		@Nullable final Project project = projectService.findOne(sessionDTO.getUserId(), projectDTO.getId());
		return (project == null) ? null : project.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public ProjectDTO updateProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "project") final ProjectDTO projectDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		if (projectDTO == null) throw new InterruptedOperationException();
		projectDTO.setUserId(sessionDTO.getUserId());
		projectService.merge(projectDTO.transformToEntity(serviceLocator));
		@Nullable final Project project = projectService.findOne(sessionDTO.getUserId(), projectDTO.getId());
		return (project == null) ? null : project.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public ProjectDTO findOneProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@Nullable final Project project = projectService.findOne(sessionDTO.getUserId(), projectId);
		return (project == null) ? null : project.transformToDTO();
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findProjectByName(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectName") final String projectName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findByName(sessionDTO.getUserId(), projectName);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findProjectByDescription(
			@WebParam(name = "session") final String token,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findByDescription(sessionDTO.getUserId(), description);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findProjectByNameOrDesc(
			@WebParam(name = "session") final String token,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findByNameOrDesc(sessionDTO.getUserId(), searchPhrase);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findAllProjects(
			@WebParam(name = "session") final String token,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findAll(sessionDTO.getUserId(), comparatorName);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@Override
	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		projectService.removeAll(sessionDTO.getUserId());
	}

	@Override
	@WebMethod
	public void removeProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		projectService.remove(sessionDTO.getUserId(), projectId);
	}

}
