package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.TaskEndpoint;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.dto.SessionDTO;
import ru.ahmetahunov.tm.dto.TaskDTO;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

	private ServiceLocator serviceLocator;

	private ITaskService taskService;

	private IPropertyService propertyService;

	private ISessionService sessionService;

	public TaskEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.taskService = serviceLocator.getTaskService();
		this.propertyService = serviceLocator.getPropertyService();
		this.sessionService = serviceLocator.getSessionService();
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public TaskDTO createTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "task") final TaskDTO taskDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		if (taskDTO == null) throw new InterruptedOperationException();
		taskDTO.setUserId(sessionDTO.getUserId());
		taskService.persist(taskDTO.transformToTask(serviceLocator));
		@Nullable final Task task = taskService.findOne(sessionDTO.getUserId(), taskDTO.getId());
		return (task == null) ? null : task.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public TaskDTO updateTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "task") final TaskDTO taskDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		if (taskDTO == null) throw new InterruptedOperationException();
		taskDTO.setUserId(sessionDTO.getUserId());
		taskService.merge(taskDTO.transformToTask(serviceLocator));
		@Nullable final Task task = taskService.findOne(sessionDTO.getUserId(), taskDTO.getId());
		return (task == null) ? null : task.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public TaskDTO findOneTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@Nullable final Task task = taskService.findOne(sessionDTO.getUserId(), taskId);
		return (task == null) ? null : task.transformToDTO();
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findTaskByName(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskName") final String taskName
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findByName(sessionDTO.getUserId(), taskName);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findTaskByDescription(
			@WebParam(name = "session") final String token,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findByDescription(sessionDTO.getUserId(), description);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findTaskByNameOrDesc(
			@WebParam(name = "session") final String token,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findByNameOrDesc(sessionDTO.getUserId(), searchPhrase);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findAllTasks(
			@WebParam(name = "session") final String token,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findAll(sessionDTO.getUserId(), comparatorName);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findAllProjectTasks(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findAll(sessionDTO.getUserId(), projectId, "name");
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@Override
	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(sessionDTO);
		serviceLocator.getTaskService().removeAll(sessionDTO.getUserId());
	}

	@Override
	@WebMethod
	public void removeTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(sessionDTO);
		serviceLocator.getTaskService().remove(sessionDTO.getUserId(), taskId);
	}

}
