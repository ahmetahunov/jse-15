package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.constant.AppConst;
import ru.ahmetahunov.tm.endpoint.*;
import ru.ahmetahunov.tm.entity.*;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator, EntityManagerService, IPropertyService {

    @NotNull
    private final Map<String, Property> propertyMap = new HashMap<>();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointImpl(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpointImpl(this);

    @Getter
    @Nullable
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("taskmanager");;

    public void init() throws Exception {
        System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");
        loadProperties();
        createDefaultUsers();
        startServer();
    }

    private void loadProperties() throws IOException {
        @NotNull final Properties properties = new Properties();
        @NotNull final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        @Nullable final InputStream fis = classLoader.getResourceAsStream("application.properties");
        properties.load(fis);
        @NotNull final Set<String> names = properties.stringPropertyNames();
        for (@NotNull final String name : names) {
            @NotNull final Property property = new Property();
            property.setName(name);
            property.setValue(properties.getProperty(name));
            propertyMap.put(property.getName(), property);
        }
        fis.close();
    }

    private void createDefaultUsers() throws AccessForbiddenException, InterruptedOperationException {
        if (!userService.contains("user")) {
            @NotNull final User user = new User();
            user.setId("64a8bae7-e916-4d1d-9b1e-04bc19d0bd91");
            user.setLogin(propertyMap.get(AppConst.USER_LOGIN).getValue());
            user.setPassword(PassUtil.getHash(propertyMap.get(AppConst.USER_PASSWORD).getValue()));
            userService.persist(user);
        }
        if (!userService.contains("admin")) {
            @NotNull final User admin = new User();
            admin.setId("3d8043c1-6aeb-4df9-af8d-7a8de0f99a09");
            admin.setLogin(propertyMap.get(AppConst.ADMIN_LOGIN).getValue());
            admin.setPassword(PassUtil.getHash(propertyMap.get(AppConst.ADMIN_PASSWORD).getValue()));
            admin.setRole(Role.ADMINISTRATOR);
            userService.persist(admin);
        }
    }

    private void startServer() {
        Endpoint.publish(AppConst.USER_ENDPOINT, userEndpoint);
        System.out.println(AppConst.USER_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.SESSION_ENDPOINT, sessionEndpoint);
        System.out.println(AppConst.SESSION_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.TASK_ENDPOINT, taskEndpoint);
        System.out.println(AppConst.TASK_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.PROJECT_ENDPOINT, projectEndpoint);
        System.out.println(AppConst.PROJECT_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.ADMIN_ENDPOINT, adminEndpoint);
        System.out.println(AppConst.ADMIN_ENDPOINT + " is running...");
    }

    @NotNull
    @Override
    public String getSecretPhrase() {
        return propertyMap.get(AppConst.SECRET_PHRASE).getValue();
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return this;
    }

}
