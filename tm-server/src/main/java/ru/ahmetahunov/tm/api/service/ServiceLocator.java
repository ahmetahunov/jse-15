package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public ISessionService getSessionService();

    @NotNull
    public IPropertyService getPropertyService();

}
