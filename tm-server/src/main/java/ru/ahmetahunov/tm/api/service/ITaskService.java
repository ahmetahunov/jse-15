package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    @Nullable
    public Task findOne(String userId, String taskId);

    @NotNull
    public List<Task> findAll(String userId);

    @NotNull
    public List<Task> findAll(String userId, String comparator);

    @NotNull
    public List<Task> findAll(String userId, String projectId, String comparator);

    @NotNull
    public List<Task> findByName(String userId, String taskName);

    @NotNull
    public List<Task> findByDescription(String userId, String description);

    @NotNull
    public List<Task> findByNameOrDesc(String userId, String searchPhrase);

    public void remove(String userId, String taskId) throws InterruptedOperationException;

    public void removeAll(String userId) throws InterruptedOperationException;

}
