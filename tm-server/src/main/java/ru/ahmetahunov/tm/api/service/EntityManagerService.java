package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import javax.persistence.EntityManagerFactory;

public interface EntityManagerService {

	@NotNull
	public EntityManagerFactory getEntityManagerFactory();

}
