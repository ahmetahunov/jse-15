package ru.ahmetahunov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class ProjectRepository implements IProjectRepository {

	@NotNull
	private final EntityManager entityManager;

	@Override
	public void persist(@NotNull final Project project) {
		entityManager.persist(project);
	}

	@Override
	public void merge(@NotNull final Project project) {
		entityManager.merge(project);
	}

	@Nullable
	@Override
	public Project findOne(@NotNull final String id) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.id = :id",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("id", id);
		@NotNull final List<Project> projects = query.getResultList();
		return (projects.isEmpty()) ? null : projects.get(0);
	}

	@Nullable
	@Override
	public Project findOneByUserId(@NotNull final String userId, @NotNull final String projectId) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		query.setParameter("id", projectId);
		@NotNull final List<Project> projects = query.getResultList();
		return (projects.isEmpty()) ? null : projects.get(0);
	}

	@NotNull
	@Override
	public List<Project> findAll() {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Project> findAllByUserId(@NotNull final String userId) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.user.id = :userId",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Project> findAllWithComparator(@NotNull final String userId, @NotNull final String comparator) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p." + comparator,
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Project> findByName(@NotNull final String userId, @NotNull final String projectName) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.user.id = :userId AND p.name LIKE :name ORDER BY p.name",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		query.setParameter("name", projectName);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Project> findByDescription(@NotNull final String userId, @NotNull final String description) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.user.id = :userId AND p.description LIKE :description ORDER BY p.description",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		query.setParameter("description", description);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Project> findByNameOrDesc(@NotNull final String userId, @NotNull final String searchPhrase) {
		@NotNull final TypedQuery<Project> query = entityManager.createQuery(
				"SELECT p FROM Project p WHERE p.user.id = :userId AND (p.name LIKE :phrase OR p.description LIKE :phrase)",
				Project.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		query.setParameter("phrase", searchPhrase);
		return query.getResultList();
	}

	@Override
	public void removeAll(@NotNull final String userId) {
		for (@NotNull final Project project : findAllByUserId(userId))
			entityManager.remove(project);
	}

	@Override
	public void removeById(@NotNull final String id) throws InterruptedOperationException {
		@Nullable final Project project = findOne(id);
		if (project == null) throw new InterruptedOperationException();
		entityManager.remove(project);
	}

	@Override
	public void remove(@NotNull final String userId, @NotNull final String id) throws InterruptedOperationException {
		@Nullable final Project project = findOneByUserId(userId, id);
		if (project == null) throw new InterruptedOperationException();
		entityManager.remove(project);
	}

}
