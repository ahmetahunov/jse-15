package ru.ahmetahunov.tm.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.endpoint.*;
import ru.ahmetahunov.tm.util.DateUtil;
import java.util.List;

public class TaskEndpointIT {

	@NotNull
	private static final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();

	@NotNull
	private static final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

	@NotNull
	private static final ProjectEndpoint projectEndpoint = new ProjectEndpointImplService().getProjectEndpointImplPort();

	@NotNull
	private static final AdminEndpoint adminEndpoint = new AdminEndpointImplService().getAdminEndpointImplPort();

	@NotNull
	private static final TaskEndpoint taskEndpoint = new TaskEndpointImplService().getTaskEndpointImplPort();

	@Nullable
	private static String tokenAdmin;

	@Nullable
	private static ProjectDTO project;

	@Nullable
	private static UserDTO user1;

	@Nullable
	private static UserDTO user2;

	@Nullable
	private static String token1;

	@Nullable
	private static String token2;

	@BeforeClass
	public static void init() throws Exception {
		user1 = userEndpoint.createUser("user1", "user1");
		user2 = userEndpoint.createUser("user2", "user2");
		token1 = sessionEndpoint.createSession("user1", "user1");
		token2 = sessionEndpoint.createSession("user2", "user2");
		@NotNull final ProjectDTO project1 = new ProjectDTO();
		project1.setName("testProject");
		project = projectEndpoint.createProject(token1, project1);
		tokenAdmin = sessionEndpoint.createSession("admin", "admin");
	}

	@AfterClass
	public static void clean() throws Exception {
		adminEndpoint.userRemove(tokenAdmin, user1.getId());
		adminEndpoint.userRemove(tokenAdmin, user2.getId());
		sessionEndpoint.removeSession(tokenAdmin);
	}

	@After
	public void clearTasks() throws Exception {
		taskEndpoint.removeAllTasks(token1);
		taskEndpoint.removeAllTasks(token2);
	}

	@Test
	public void createTaskExceptionTest() {
		@NotNull final TaskDTO task = new TaskDTO();
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.createTask(null, task)
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.createTask("", task)
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.createTask(token1, null)
		);
	}

	@Test
	public void createTaskOkTest() throws Exception {
		@NotNull final TaskDTO task = new TaskDTO();
		task.setName("testTask");
		task.setProjectId(project.getId());
		@NotNull final TaskDTO created = taskEndpoint.createTask(token1, task);
		@Nullable final TaskDTO found = taskEndpoint.findOneTask(token1, created.getId());
		Assert.assertNotNull(found);
		Assert.assertNotNull(created);
		Assert.assertEquals(created.getId(), found.getId());
		Assert.assertEquals(created.getName(), found.getName());
	}

	@Test
	public void updateTaskExceptionTest() {
		@NotNull final TaskDTO task = new TaskDTO();
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.updateTask(null, task)
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.updateTask("", task)
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.updateTask(token1, null)
		);
	}

	@Test
	public void updateTaskOkTest() throws Exception {
		@NotNull final TaskDTO task = new TaskDTO();
		task.setName("updated");
		task.setProjectId(project.getId());
		@NotNull final TaskDTO created = taskEndpoint.createTask(token1, task);
		created.setName("testTask");
		taskEndpoint.updateTask(token1, created);
		@Nullable final TaskDTO found = taskEndpoint.findOneTask(token1, created.getId());
		Assert.assertEquals("testTask", found.getName());
	}

	@Test
	public void findAllTasksExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findAllTasks(null, "name")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findAllTasks("", "name")
		);
	}

	@Test
	public void findAllTasksSortTest() throws Exception {
		@NotNull final TaskDTO task1 = new TaskDTO();
		task1.setName("test1");
		task1.setProjectId(project.getId());
		task1.setFinishDate(DateUtil.parseDate("11.11.2011"));
		taskEndpoint.createTask(token1, task1);
		Thread.sleep(1000);
		@NotNull final TaskDTO task2 = new TaskDTO();
		task2.setName("test3");
		task2.setProjectId(project.getId());
		task2.setStatus(Status.DONE);
		taskEndpoint.createTask(token1, task2);
		Thread.sleep(1000);
		@NotNull final TaskDTO task3 = new TaskDTO();
		task3.setName("test2");
		task3.setProjectId(project.getId());
		task3.setStartDate(DateUtil.parseDate("11.11.2011"));
		taskEndpoint.createTask(token1, task3);
		@NotNull final List<TaskDTO> found = taskEndpoint.findAllTasks(token1, "creationDate");
		Assert.assertEquals("test1", found.get(0).getName());
		Assert.assertEquals("test3", found.get(1).getName());
		Assert.assertEquals("test2", found.get(2).getName());
		@NotNull final List<TaskDTO> found1 = taskEndpoint.findAllTasks(token1, null);
		Assert.assertEquals("test1", found1.get(0).getName());
		Assert.assertEquals("test2", found1.get(1).getName());
		Assert.assertEquals("test3", found1.get(2).getName());
		@NotNull final List<TaskDTO> found2 = taskEndpoint.findAllTasks(token1, "startDate");
		Assert.assertEquals("test2", found2.get(2).getName());
		@NotNull final List<TaskDTO> found3 = taskEndpoint.findAllTasks(token1, "finishDate");
		Assert.assertEquals("test1", found3.get(2).getName());
		@NotNull final List<TaskDTO> found4 = taskEndpoint.findAllTasks(token1, "status");
		Assert.assertEquals("test3", found4.get(0).getName());
	}

	@Test
	public void findAllTasksOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final TaskDTO task = new TaskDTO();
			task.setName("test" + i);
			task.setProjectId(project.getId());
			taskEndpoint.createTask(token1, task);
		}
		@NotNull final List<TaskDTO> found = taskEndpoint.findAllTasks(token1, null);
		Assert.assertEquals(11, found.size());
		@NotNull final List<TaskDTO> found1 = taskEndpoint.findAllTasks(token2, null);
		Assert.assertEquals(0, found1.size());
	}

	@Test
	public void findAllProjectExceptionTasksTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findAllProjectTasks(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findAllProjectTasks("", "")
		);
	}

	@Test
	public void findAllProjectTasksOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final TaskDTO task = new TaskDTO();
			task.setName("test" + i);
			task.setProjectId(project.getId());
			taskEndpoint.createTask(token1, task);
		}
		@NotNull final List<TaskDTO> found = taskEndpoint.findAllProjectTasks(token1, project.getId());
		Assert.assertEquals(11, found.size());
		@NotNull final List<TaskDTO> found1 = taskEndpoint.findAllProjectTasks(token2, project.getId());
		Assert.assertEquals(0, found1.size());
		@NotNull final List<TaskDTO> found2 = taskEndpoint.findAllProjectTasks(token1, "");
		Assert.assertEquals(0, found2.size());
		@NotNull final List<TaskDTO> found3 = taskEndpoint.findAllProjectTasks(token1, null);
		Assert.assertEquals(0, found3.size());
	}

	@Test
	public void findOneTaskExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findOneTask(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findOneTask("", "")
		);
	}


	@Test
	public void findOneTaskOkTest() throws Exception {
		@NotNull final TaskDTO task = new TaskDTO();
		task.setName("testTask");
		task.setProjectId(project.getId());
		@NotNull final TaskDTO created = taskEndpoint.createTask(token1, task);
		@Nullable final TaskDTO found = taskEndpoint.findOneTask(token1, created.getId());
		Assert.assertEquals(created.getId(), found.getId());
		Assert.assertEquals(created.getName(), found.getName());
		@Nullable final TaskDTO found1 = taskEndpoint.findOneTask(token2, created.getId());
		Assert.assertNull(found1);
		@Nullable final TaskDTO found2 = taskEndpoint.findOneTask(token1, "");
		Assert.assertNull(found2);
		@Nullable final TaskDTO found3 = taskEndpoint.findOneTask(token1, null);
		Assert.assertNull(found3);
	}

	@Test
	public void findTaskByNameExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findTaskByName(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findTaskByName("", "")
		);
	}

	@Test
	public void findTaskByNameOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final TaskDTO task = new TaskDTO();
			task.setName("test" + i);
			task.setProjectId(project.getId());
			taskEndpoint.createTask(token1, task);
		}
		@NotNull final List<TaskDTO> found = taskEndpoint.findTaskByName(token1, "1");
		Assert.assertEquals(2, found.size());
		@NotNull final List<TaskDTO> found1 = taskEndpoint.findTaskByName(token1, "2");
		Assert.assertEquals(1, found1.size());
		@NotNull final List<TaskDTO> found2 = taskEndpoint.findTaskByName(token1, "t");
		Assert.assertEquals(11, found2.size());
		@NotNull final List<TaskDTO> found3 = taskEndpoint.findTaskByName(token2, "t");
		Assert.assertEquals(0, found3.size());
		@NotNull final List<TaskDTO> found4 = taskEndpoint.findTaskByName(token1, "11");
		Assert.assertEquals(0, found4.size());
		@NotNull final List<TaskDTO> found5 = taskEndpoint.findTaskByName(token1, "");
		Assert.assertEquals(0, found5.size());
		@NotNull final List<TaskDTO> found6 = taskEndpoint.findTaskByName(token1, null);
		Assert.assertEquals(0, found6.size());
	}

	@Test
	public void findTaskByDescriptionExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findTaskByDescription(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findTaskByDescription("", "")
		);
	}

	@Test
	public void findTaskByDescriptionOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final TaskDTO task = new TaskDTO();
			task.setName("test" + i);
			task.setDescription("description" + i);
			task.setProjectId(project.getId());
			taskEndpoint.createTask(token1, task);
		}
		@NotNull final List<TaskDTO> found = taskEndpoint.findTaskByDescription(token1, "n1");
		Assert.assertEquals(2, found.size());
		@NotNull final List<TaskDTO> found1 = taskEndpoint.findTaskByDescription(token1, "n2");
		Assert.assertEquals(1, found1.size());
		@NotNull final List<TaskDTO> found2 = taskEndpoint.findTaskByDescription(token1, "d");
		Assert.assertEquals(11, found2.size());
		@NotNull final List<TaskDTO> found3 = taskEndpoint.findTaskByDescription(token2, "d");
		Assert.assertEquals(0, found3.size());
		@NotNull final List<TaskDTO> found4 = taskEndpoint.findTaskByDescription(token1, "n11");
		Assert.assertEquals(0, found4.size());
		@NotNull final List<TaskDTO> found5 = taskEndpoint.findTaskByDescription(token1, "");
		Assert.assertEquals(0, found5.size());
		@NotNull final List<TaskDTO> found6 = taskEndpoint.findTaskByDescription(token1, null);
		Assert.assertEquals(0, found6.size());
	}

	@Test
	public void findTaskByNameOrDescExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findTaskByNameOrDesc(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.findTaskByNameOrDesc("", "")
		);
	}

	@Test
	public void findTaskByNameOrDescOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final TaskDTO task = new TaskDTO();
			task.setName("test" + i);
			task.setDescription("description" + i);
			task.setProjectId(project.getId());
			taskEndpoint.createTask(token1, task);
		}
		@NotNull final List<TaskDTO> found = taskEndpoint.findTaskByNameOrDesc(token1, "1");
		Assert.assertEquals(2, found.size());
		@NotNull final List<TaskDTO> found1 = taskEndpoint.findTaskByNameOrDesc(token1, "2");
		Assert.assertEquals(1, found1.size());
		@NotNull final List<TaskDTO> found2 = taskEndpoint.findTaskByNameOrDesc(token1, "t");
		Assert.assertEquals(11, found2.size());
		@NotNull final List<TaskDTO> found3 = taskEndpoint.findTaskByNameOrDesc(token2, "d");
		Assert.assertEquals(0, found3.size());
		@NotNull final List<TaskDTO> found4 = taskEndpoint.findTaskByNameOrDesc(token1, "11");
		Assert.assertEquals(0, found4.size());
		@NotNull final List<TaskDTO> found5 = taskEndpoint.findTaskByNameOrDesc(token1, "");
		Assert.assertEquals(0, found5.size());
		@NotNull final List<TaskDTO> found6 = taskEndpoint.findTaskByNameOrDesc(token1, null);
		Assert.assertEquals(0, found6.size());
	}

	@Test
	public void removeAllTasksExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.removeAllTasks(null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.removeAllTasks("")
		);
	}

	@Test
	public void removeAllTasksOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final TaskDTO task = new TaskDTO();
			task.setName("test" + i);
			task.setDescription("description" + i);
			task.setProjectId(project.getId());
			taskEndpoint.createTask(token1, task);
		}
		taskEndpoint.removeAllTasks(token1);
		@NotNull final List<TaskDTO> found = taskEndpoint.findAllTasks(token1, null);
		Assert.assertEquals(0, found.size());
	}

	@Test
	public void removeTaskExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.removeTask(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> taskEndpoint.removeTask("", "")
		);
	}

	@Test
	public void removeTaskOkTest() throws Exception {
		@NotNull final TaskDTO task = new TaskDTO();
		task.setName("testTask");
		task.setProjectId(project.getId());
		@Nullable final TaskDTO created = taskEndpoint.createTask(token1, task);
		Assert.assertNotNull(created);
		taskEndpoint.removeTask(token1, created.getId());
		@Nullable final TaskDTO found = taskEndpoint.findOneTask(token1, created.getId());
		Assert.assertNull(found);
	}

}
