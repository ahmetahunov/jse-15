package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.endpoint.*;

public interface ServiceLocator {

    @NotNull
    public IStateService getStateService();

    @NotNull
    public ITerminalService getTerminalService();

    @NotNull
    public SessionEndpoint getSessionEndpoint();

    @NotNull
    public UserEndpoint getUserEndpoint();

    @NotNull
    public ProjectEndpoint getProjectEndpoint();

    @NotNull
    public TaskEndpoint getTaskEndpoint();

    @NotNull
    public AdminEndpoint getAdminEndpoint();

}
