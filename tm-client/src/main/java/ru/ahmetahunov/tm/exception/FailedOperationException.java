package ru.ahmetahunov.tm.exception;

public final class FailedOperationException extends Exception {

	public FailedOperationException(final String message) { super(message); }

}
