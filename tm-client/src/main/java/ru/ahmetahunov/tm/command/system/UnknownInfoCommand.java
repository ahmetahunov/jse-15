package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UnknownInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "unknown";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Notifies about unknown operation.";
    }

    @Override
    public void execute() {
        serviceLocator.getTerminalService().writeMessage("Unknown operation. Please enter help for help.");
    }

}
