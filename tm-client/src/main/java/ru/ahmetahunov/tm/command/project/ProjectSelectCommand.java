package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class ProjectSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show selected project with tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECT SELECT]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist.");
        terminalService.writeMessage(project.getName() + ":");
        int i = 1;
        @NotNull final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findAllProjectTasks(session, projectId);
        for (@NotNull final TaskDTO task : tasks) {
            terminalService.writeMessage(String.format("  %d. %s", i++, task.getName()));
        }
    }

}
