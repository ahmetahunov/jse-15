package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.ProjectDTO;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.DateUtil;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[PROJECT CREATE]");
        @NotNull final String name = terminalService.getAnswer("Please enter project name: ");
        if (name.isEmpty()) throw new FailedOperationException("Name cannot be empty.");
        @NotNull final String description = terminalService.getAnswer("Please enter description:");
        @NotNull final String startDate =
                terminalService.getAnswer("Please insert start date(example: 01.01.2020): ");
        @NotNull final String finishDate =
                terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        serviceLocator.getProjectEndpoint().createProject(session, project);
        terminalService.writeMessage("[OK]");
    }

}