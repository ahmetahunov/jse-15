package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;

@NoArgsConstructor
public final class UserChangePassAdminCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "change-user-pass";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change selected user's password.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[CHANGE USER'S PASSWORD]");
        @NotNull final String userId = terminalService.getAnswer("Please enter user's id: ");
        @NotNull String password = terminalService.getAnswer("Please enter new password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        serviceLocator.getAdminEndpoint().userUpdatePasswordAdmin(session, userId, password);
        terminalService.writeMessage("[OK]");
    }

}
