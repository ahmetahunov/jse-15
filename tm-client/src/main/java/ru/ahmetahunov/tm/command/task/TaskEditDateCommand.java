package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.DateUtil;
import java.lang.Exception;

@NoArgsConstructor
public final class TaskEditDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-edit-dates";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit start and finish dates of selected task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DATES]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(session, taskId);
        if (task == null) throw new FailedOperationException("Selected task does not exist.");
        @NotNull final String startDate = terminalService.getAnswer("Please enter new start date: ");
        @NotNull final String finishDate = terminalService.getAnswer("Please enter new finish date: ");
        task.setStartDate(DateUtil.parseDate(startDate));
        task.setFinishDate(DateUtil.parseDate(finishDate));
        taskEndpoint.updateTask(session, task);
        terminalService.writeMessage("[OK]");
    }

}
