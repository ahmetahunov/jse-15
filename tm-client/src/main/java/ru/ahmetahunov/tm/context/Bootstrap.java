package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.command.*;
import ru.ahmetahunov.tm.endpoint.*;
import ru.ahmetahunov.tm.service.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Exception;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Getter
    @NotNull
    private final IStateService stateService = new StateService(commands);

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService(reader);

    @Getter
    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpointImplService().getAdminEndpointImplPort();

    @Getter
    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

    @Getter
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImplService().getProjectEndpointImplPort();

    @Getter
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImplService().getTaskEndpointImplPort();

    @Getter
    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();

    public void init() throws Exception {
        initCommands();
        startCycle();
    }

    private void initCommands() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.ahmetahunov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> commandClasses =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> commandClass : commandClasses) {
            @NotNull final AbstractCommand command = commandClass.getDeclaredConstructor().newInstance();
            command.setServiceLocator(this);
            commands.put(command.getName(), command);
        }
    }

    private void startCycle() throws IOException {
        terminalService.writeMessage( "*** WELCOME TO TASK MANAGER ***" );
        @NotNull String operation = "";
        while (!"exit".equals(operation)) {
            try {
                operation = terminalService.getAnswer("Please enter command: ").toLowerCase();
                @Nullable final AbstractCommand command = stateService.getCommand(operation);
                if (command == null) continue;
                command.execute();
            }
            catch (Exception e) { terminalService.writeMessage(e.getMessage()); }
            terminalService.writeMessage("");
        }
        terminalService.close();
    }

}
