https://gitlab.com/ahmetahunov/jse-15
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0
+ MySQL 5.5

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/jse-15.git
cd jse-15
mvn clean install
```

## run app server
```bash
java -jar tm-server/target/release/bin/tm-server.jar
```

## run app client
```bash
java -jar tm-client/target/release/bin/tm-client.jar
```

## open server docs in browser
#### windows
```
start tm-server\target\release\docs\apidocs\index.html
```

#### macOs
```
open tm-client/target/release/docs/apidocs/index.html
```

#### linux
```
xdg-open tm-client/target/release/docs/apidocs/index.html
```